using System;
using System.Collections.Generic;

namespace RedTail.Utility
{
  public static class DateTime
  {
    public static List<System.DateTime> GetDateRange(System.DateTime start, System.DateTime end)
		{
			TimeSpan timeSpan = end - start;
			List<System.DateTime> dateRange = new List<System.DateTime>();

			for (Int32 i = 0; i <= timeSpan.Days; i += 1)
			{
				dateRange.Add(start.AddDays(i));
			}

			return dateRange;
		}
  }
}
