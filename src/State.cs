using System;

using RedTail;

namespace RedTail.Utility
{
  public static class State
	{
		public static RedTail.Helpers.State Parse(String value)
		{
			foreach (RedTail.Helpers.State itmState in RedTail.Helpers.Collections.States)
			{
				if (itmState.GetValue().ToLower() == value.ToLower())
				{
					return itmState;
				}
			}

			return RedTail.Helpers.State.ER;
		}
	}
}
